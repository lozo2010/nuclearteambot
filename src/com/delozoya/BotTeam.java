package com.delozoya;

import com.delozoya.util.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.send.SendSticker;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 9/04/16.
 */
public class BotTeam extends TelegramLongPollingBot {
    String language;
    @Override
    public String getBotUsername() {
        return BuildVars.name;
    }

    @Override
    public String getBotToken() {
        return BuildVars.token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        //System.out.println(update.getMessage().getSticker());
        if(update.hasMessage()){
            Message message = update.getMessage();


            //check if the message has text. it could also  contain for example a location ( message.hasLocation() )
            if(message.hasText()){
                if (message.getText().equals(Commands.startCommand)){
                    try {
                        if(ServicesDatabase.addID(String.valueOf(message.getFrom().getId()),message.getFrom().getUserName())){
                            language = ServicesDatabase.getUserLanguage(update.getMessage().getFrom().getId());

                            try {
                                SendMessage sendMessageRequest = new SendMessage();
                                sendMessageRequest.setReplayToMessageId(message.getMessageId());
                                sendMessageRequest.setChatId(message.getChatId().toString());
                                sendMessageRequest.enableMarkdown(true);

                                sendMessageRequest.setReplayMarkup(keyboardStart());
                                sendMessageRequest.setText(Idioma.getString("start",language));
                                sendMessage(sendMessageRequest);
                            } catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try{
                                SendMessage sendMessageRequest = new SendMessage();
                                sendMessageRequest.setReplayToMessageId(message.getMessageId());
                                sendMessageRequest.setChatId(message.getChatId().toString());
                                sendMessageRequest.enableMarkdown(true);

                                sendMessageRequest.setReplayMarkup(keyboardIdioma());
                                sendMessageRequest.setText(Idioma.getString("idioma","EN"));
                                sendMessage(sendMessageRequest);
                            } catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                        System.out.println(message.getText());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }

                if(message.getText().equals("Español")){
                    ServicesDatabase.setIdioma(String.valueOf(message.getFrom().getId()),"ES");
                    language = ServicesDatabase.getUserLanguage(update.getMessage().getFrom().getId());

                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardStart());
                        sendMessageRequest.setText(Idioma.getString("start","ES"));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if(message.getText().equals("Ingles")){
                    ServicesDatabase.setIdioma(String.valueOf(message.getFrom().getId()),"EN");
                    language = ServicesDatabase.getUserLanguage(update.getMessage().getFrom().getId());

                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardStart());
                        sendMessageRequest.setText(Idioma.getString("start","EN"));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                language = ServicesDatabase.getUserLanguage(update.getMessage().getFrom().getId());
                if (message.getText().equals("OnePlus One")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("OneplusOne",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                }

                if (message.getText().equals("Zuk Z1")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardZukZ1());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


/*-------------------------------------------------------------------------------------------------------------*/


                if (message.getText().equals("OnePlus One - Radioactive Kernels")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("versionAndroid",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("OnePlus One - Roms")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardRomsOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("versionAndroid",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("OnePlus One Roms - Lollipop")){
                    ServicesDatabase.setContador("Slim");
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        SendSticker p1 = new SendSticker();
                        p1.setSticker(ResourcesStrings.imgSlim);
                        p1.setChatId(message.getChatId() + "");

                        sendMessageRequest.setReplayMarkup(keyboardRomLollipopOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                        sendSticker(p1);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Rom Lollipop - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.NuclearSlim + "\n" + ResourcesStrings.linkNuclearSlim);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Rom Lollipop - HtcMania")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.NuclearSlim + "\n" + ResourcesStrings.linkNuclearSlimHtc);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Rom Lollipop - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.NuclearSlim + "\n" + ResourcesStrings.linkNuclearSlimXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("OnePlus One Kernel - Lollipop")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);


                        sendMessageRequest.setReplayMarkup(keyboardKernelLollipop1OneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop - Radioactive Kernel")){
                    ServicesDatabase.setContador("kernelLolli");
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);


                        sendMessageRequest.setReplayMarkup(keyboardKernelLollipopOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }



                if (message.getText().equals("One+One Kernel Lollipop - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkSlimKernel);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop - HtcMania")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkSlimKernelHtc);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkSlimKernelXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkAcuicultor);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


                if (message.getText().equals("One+One Kernel Lollipop - Radioactive Kernel CAF")){
                    ServicesDatabase.setContador("kernelLolliCAF");

                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);


                        sendMessageRequest.setReplayMarkup(keyboardKernelLollipopCAFOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }



                if (message.getText().equals("One+One Kernel Lollipop CAF - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlimCAF + "\n" + ResourcesStrings.linkSlimCAFKernel);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop CAF - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkSlimCAFKernelXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel Lollipop CAF - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelSlim + "\n" + ResourcesStrings.linkAcuicultorLollipopCAF);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*----------------------------------------------------------------------------------------------*/


                if (message.getText().equals("OnePlus One Roms - Marshmallow")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);


                        sendMessageRequest.setReplayMarkup(keyboardRomMarshmallowOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("One+One Rom - NuclearRomReborn")){
                    ServicesDatabase.setContador("Nrr");

                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        SendSticker p1 = new SendSticker();
                        p1.setSticker(ResourcesStrings.imgNrr);
                        p1.setChatId(message.getChatId() + "");

                        sendMessageRequest.setReplayMarkup(keyboardRomNRROneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                        sendSticker(p1);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearRomReborn - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nrr + "\n" + ResourcesStrings.linkNRR);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearRomReborn - HtcMania")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nrr + "\n" + ResourcesStrings.linkNRRHtc);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearRomReborn - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nrr + "\n" + ResourcesStrings.linkNRRXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearRomReborn - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nrr + "\n" + ResourcesStrings.linkNRRGitHub);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


                if (message.getText().equals("One+One Rom - NuclearAndroidProject")){
                    ServicesDatabase.setContador("Nap");

                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        SendSticker p1 = new SendSticker();
                        p1.setSticker(ResourcesStrings.imgNap);
                        p1.setChatId(message.getChatId() + "");

                        sendMessageRequest.setReplayMarkup(keyboardRomNAPOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                        sendSticker(p1);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("NuclearAndroidProject - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nap+ "\n" + ResourcesStrings.linkNAP);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearAndroidProject - HtcMania")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nap + "\n" + ResourcesStrings.linkNAPHtc);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearAndroidProject - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nap + "\n" + ResourcesStrings.linkNAPXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("NuclearAndroidProject - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Nap + "\n" + ResourcesStrings.linkNAPGitHub);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("OnePlus One Kernel - Marshmallow")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelMarshmallowOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("One+One Kernel - Radioactive Reborn Kernel")){
                    ServicesDatabase.setContador("kernelMarsh");
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelMarshmallow1OneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive Reborn Kernel - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelNrr+ "\n" + ResourcesStrings.linkRebornKernel);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive Reborn Kernel - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelNrr + "\n" + ResourcesStrings.linkRebornKernelXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive Reborn Kernel - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelNrr + "\n" + ResourcesStrings.linkAcuicultorReborn);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }



                if (message.getText().equals("One+One Kernel - Radioactive Kernel CAF")){
                    ServicesDatabase.setContador("kernelMarshCAF");
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelMarshmallowCAFOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


                if (message.getText().equals("Radioactive Marshmallow CAF Kernel - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelCAF+ "\n" + ResourcesStrings.linkCafKernel);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive Marshmallow CAF Kernel - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelCAF + "\n" + ResourcesStrings.linkCafKernelXDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive Marshmallow CAF Kernel - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.KernelCAF + "\n" + ResourcesStrings.linkAcuicultorCaf);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }





/*-------------------------------------------------------------------------------------------------------------*/

                if (message.getText().equals("Cancel") || message.getText().equals("Cancelar")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardStart());
                        sendMessageRequest.setText(Idioma.getString("start",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Menu")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("OneplusOne",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Roms")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardRomsOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("versionAndroid",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Kernel")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelLollipop1OneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Kernel Marshmallow")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardKernelMarshmallowOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" 1+1 Roms Marshmallow")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardRomMarshmallowOneplusOne());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

/*----------------------------------------------------------------------------------*/
                if (message.getText().equals("Other") || message.getText().equals("Otros")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardOther());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Recovery Oneplus One")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.recoveryOneplusOne);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Firmware Oneplus One")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.firmwareOneplusOne);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Recovery Zuk Z1")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.recoveryzukz1);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Firmware Zuk Z1")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.firmwarezukz1);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("App Wallpaper NuclearTeam")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        

                        sendMessageRequest.setText(ResourcesStrings.appWallpaper);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("About Team")||message.getText().equals("Acerca del Team")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        //sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setText(ResourcesStrings.Team);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if(message.getText().equals("Idioma")||message.getText().equals("Language")){
                    try{
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardIdioma());
                        sendMessageRequest.setText(Idioma.getString("idioma",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if(message.getText().equals("Zuk Z1 - Radioactive Kernel")){
                    ServicesDatabase.setContador("ZukZ1Kernel");
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardZukKernel());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive HAM Kernel - Link")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());


                        sendMessageRequest.setText(ResourcesStrings.linkZukZ1Kernel);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive HAM Kernel - XDADeveloper")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());


                        sendMessageRequest.setText(ResourcesStrings.linkZukZ1XDA);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals("Radioactive HAM Kernel - GitHub")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());


                        sendMessageRequest.setText(ResourcesStrings.linkZukZ1GitHub);
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

                if (message.getText().equals(Idioma.getString("Back to",language)+" Zuk Z1 Menu")){
                    try {
                        SendMessage sendMessageRequest = new SendMessage();
                        sendMessageRequest.setReplayToMessageId(message.getMessageId());
                        sendMessageRequest.setChatId(message.getChatId().toString());
                        sendMessageRequest.enableMarkdown(true);

                        sendMessageRequest.setReplayMarkup(keyboardZukZ1());
                        sendMessageRequest.setText(Idioma.getString("necesitas",language));
                        sendMessage(sendMessageRequest);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }



            }//end if()
        }//end  if()
    }

    public ReplyKeyboardMarkup keyboardStart(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardButton b1 = new KeyboardButton("OnePlus One");
        KeyboardButton b2 = new KeyboardButton("Zuk Z1");

        KeyboardButton b3 = new KeyboardButton(Idioma.getString("About",language));
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Other",language));
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Language",language));
        KeyboardButton b6 = new KeyboardButton(Idioma.getString("Cancel",language));
        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow1.add(b4);
        keyrow2.add(b5);
        keyrow2.add(b6);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardIdioma(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyrow = new KeyboardRow();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardButton b1 = new KeyboardButton("Español");

        KeyboardButton b2 = new KeyboardButton("Ingles");
        keyrow.add(b1);
        keyrow.add(b2);

        keyboard.add(keyrow);


        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardButton b1 = new KeyboardButton("OnePlus One - Radioactive Kernels");
        KeyboardButton b2 = new KeyboardButton("OnePlus One - Roms");

        keyrow.add(b1);
        keyrow.add(b2);
        KeyboardButton b3 = new KeyboardButton(Idioma.getString("Cancel",language));
        keyrow1.add(b3);

        keyboard.add(keyrow);
        keyboard.add(keyrow1);


        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardZukZ1(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("Zuk Z1 - Radioactive Kernel");
        KeyboardButton b2 = new KeyboardButton("Zuk Z1 - Roms");
        KeyboardButton b3 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);

        keyboard.add(keyrow);
        keyboard.add(keyrow1);


        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("OnePlus One Kernel - Lollipop");
        KeyboardButton b2 = new KeyboardButton("OnePlus One Kernel - Marshmallow");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Menu");
        KeyboardButton b3 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b4);
        keyrow1.add(b3);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardRomsOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("OnePlus One Roms - Lollipop");
        KeyboardButton b2 = new KeyboardButton("OnePlus One Roms - Marshmallow");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Menu");
        KeyboardButton b3 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b4);
        keyrow1.add(b3);

        keyboard.add(keyrow);
        keyboard.add(keyrow1);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelLollipopOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();





        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Kernel Lollipop - Link");
        KeyboardButton b2 = new KeyboardButton("One+One Kernel Lollipop - HtcMania");
        KeyboardButton b3 = new KeyboardButton("One+One Kernel Lollipop - XDADeveloper");
        KeyboardButton b4 = new KeyboardButton("One+One Kernel Lollipop - GitHub");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b6 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow1.add(b4);
        keyrow2.add(b5);
        keyrow2.add(b6);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelLollipop1OneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Kernel Lollipop - Radioactive Kernel");
        KeyboardButton b2 = new KeyboardButton("One+One Kernel Lollipop - Radioactive Kernel CAF");
        KeyboardButton b3 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel");
        KeyboardButton b4 = new KeyboardButton((Idioma.getString("Cancel",language)));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow2.add(b3);
        keyrow2.add(b4);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelLollipopCAFOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Kernel Lollipop CAF - Link");
        KeyboardButton b2 = new KeyboardButton("One+One Kernel Lollipop CAF - XDADeveloper");
        KeyboardButton b3 = new KeyboardButton("One+One Kernel Lollipop CAF - GitHub");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow1.add(b3);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardRomLollipopOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Rom Lollipop - Link");
        KeyboardButton b2 = new KeyboardButton("One+One Rom Lollipop - HtcMania");
        KeyboardButton b3 = new KeyboardButton("One+One Rom Lollipop - XDADeveloper");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardRomMarshmallowOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Rom - NuclearRomReborn");
        KeyboardButton b2 = new KeyboardButton("One+One Rom - NuclearAndroidProject");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardRomNRROneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("NuclearRomReborn - Link");
        KeyboardButton b2 = new KeyboardButton("NuclearRomReborn - HtcMania");
        KeyboardButton b3 = new KeyboardButton("NuclearRomReborn - GitHub");
        KeyboardButton b6 = new KeyboardButton("NuclearRomReborn - XDADeveloper");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow1.add(b6);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardRomNAPOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("NuclearAndroidProject - Link");
        KeyboardButton b2 = new KeyboardButton("NuclearAndroidProject - HtcMania");
        KeyboardButton b3 = new KeyboardButton("NuclearAndroidProject - GitHub");
        KeyboardButton b6 = new KeyboardButton("NuclearAndroidProject - XDADeveloper");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow1.add(b6);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);


        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelMarshmallowOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("One+One Kernel - Radioactive Reborn Kernel");
        KeyboardButton b2 = new KeyboardButton("One+One Kernel - Radioactive Kernel CAF");

        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);


        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelMarshmallow1OneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("Radioactive Reborn Kernel - Link");
        KeyboardButton b2 = new KeyboardButton("Radioactive Reborn Kernel - XDADeveloper");
        KeyboardButton b3 = new KeyboardButton("Radioactive Reborn Kernel - GitHub");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow1.add(b3);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardKernelMarshmallowCAFOneplusOne(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("Radioactive Marshmallow CAF Kernel - Link");
        KeyboardButton b2 = new KeyboardButton("Radioactive Marshmallow CAF Kernel - XDADeveloper");
        KeyboardButton b3 = new KeyboardButton("Radioactive Marshmallow CAF Kernel - GitHub");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" 1+1 Kernel Lollipop");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow1.add(b3);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardOther(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("Recovery Oneplus One");
        KeyboardButton b2 = new KeyboardButton("Firmware Oneplus One");
        KeyboardButton b3 = new KeyboardButton("Recovery Zuk Z1");
        KeyboardButton b4 = new KeyboardButton("Firmware Zuk Z1");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow.add(b2);
        keyrow1.add(b3);
        keyrow1.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup keyboardZukKernel(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow keyrow = new KeyboardRow();
        KeyboardRow keyrow1 = new KeyboardRow();
        KeyboardRow keyrow2 = new KeyboardRow();

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardButton b1 = new KeyboardButton("Radioactive HAM Kernel - Link");
        KeyboardButton b2 = new KeyboardButton("Radioactive HAM Kernel - XDADeveloper");
        KeyboardButton b3 = new KeyboardButton("Radioactive HAM Kernel - GitHub");
        KeyboardButton b4 = new KeyboardButton(Idioma.getString("Back to",language)+" Zuk Z1 Menu");
        KeyboardButton b5 = new KeyboardButton(Idioma.getString("Cancel",language));

        keyrow.add(b1);
        keyrow1.add(b2);
        keyrow1.add(b3);
        keyrow2.add(b4);
        keyrow2.add(b5);
        keyboard.add(keyrow);
        keyboard.add(keyrow1);
        keyboard.add(keyrow2);

        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

}
