package com.delozoya.util;

/**
 * Created by David on 03/02/2016.
 */
public class ResourcesStrings {
    public static final String cambiaridiomaEN ="Language";
    public static final String cambiaridiomaES ="Idioma";

    public static final String startEN ="What Device?";
    public static final String startES ="¿Qué Dispositivo?";

    public static final String idiomaEN ="What language do you need?";
    public static final String idiomaES ="¿Qué idioma quieres usar?";

    public static final String OneplusOneEN ="What do you need?";
    public static final String OneplusOneES ="¿Qué necesitas?";


    public static final String ZukZ1EN ="What do you need?";

    public static final String versionAndroidEN = "What Android Version do you need?";
    public static final String versionAndroidES = "¿Qué Versión de Android quieres?";


    public static final String necesitasEN ="What do you need?";
    public static final String necesitasES ="¿Qué necesitas?";

    public static final String cancelEN ="Cancel";
    public static final String cancelES ="Cancelar";

    public static final String otherEN ="Other";
    public static final String otherES ="Otros";

    public static final String aboutEN ="About Team";
    public static final String aboutES ="Acerca del Team";

    public static final String backEN ="Back to";
    public static final String backES ="Ir a";


    public static final String Team = "David(@lozoab and lozo2010) - Rom, App and Bot Developer. \n" +
            "Alex(@moludo) - Rom Developer. \n" +
            "Ramon(@acuicultor) - Rom Developer Kernel. \n" +
            "Chris(@T3CHNO) - Maintainer. \n" +
            "Victoria(@VictoriaMA) - Maintainer. \n" +
            "Armallones(@armajohnes) - Tester. \n" +
            "Sonia(@sonifer) - Tester. \n" +
            "Alfo(@alfo2) - Tester. \n" +
            "Santos(@Gusy_80) - Tester. \n" +
            "Dtr(@Dtrworld) - Tester. \n" +
            "Jesus(@JesusGo) - Tester. \n" ;

    public static final String NuclearSlim = "NuclearSlim";
    public static final String Nrr = "NuclearRomReborn";

    public static final String Nap = "NuclearAndroidProject";

    public static final String KernelSlim = "Radioactive Lollipop Kernel";
    public static final String KernelSlimCAF = "Radioactive Lollipop Kernel CAF";

    public static final String KernelNrr = "Radioactive Reborn Kernel";
    public static final String KernelCAF = "Radioactive Reborn Kernel CAF";

    public static final String imgNap = "BQADBAADTAcAArIFDgAB-7fUpm_DnQEC";
    public static final String imgNrr = "BQADBAADKAkAArIFDgABF-VCZg739DkC";
    public static final String imgSlim = "BQADBAADnAUAArIFDgABSsOPSVM-icIC";
    public static final String imgTeam = "BQADBAADmAUAArIFDgABARcIOTYPqEcC";

    public static final String linkZukZ1Kernel ="https://www.androidfilehost.com/?w=files&flid=53988";
    public static final String linkZukZ1XDA ="http://forum.xda-developers.com/zuk-z1/orig-development/radioactive-kernel-linaro-graphite-t3356611";
    public static final String linkZukZ1GitHub ="https://github.com/acuicultor/Radioactive-kernel-HAM";


    //Links
    public static final String linkNuclearSlim ="https://www.androidfilehost.com/?w=files&flid=39515";
    public static final String linkNuclearSlimHtc = "http://www.htcmania.com/showthread.php?t=1034669";
    public static final String linkNuclearSlimXDA = "http://forum.xda-developers.com/oneplus-one/development/nuclearslim-t3133741";

    public static final String linkNRR ="https://www.androidfilehost.com/?w=files&flid=45109";
    public static final String linkNRRHtc = "http://www.htcmania.com/showthread.php?t=1109696";
    public static final String linkNRRXDA = "http://forum.xda-developers.com/oneplus-one/development/rom-kernel-nuclearrom-reborn-t3266738";
    public static final String linkNRRGitHub = "http://github.com/NuclearCm";

    public static final String linkNAP ="https://www.androidfilehost.com/?w=files&flid=43400";
    public static final String linkNAPHtc = "http://www.htcmania.com/showthread.php?t=1097834";
    public static final String linkNAPXDA = "http://forum.xda-developers.com/oneplus-one/development/6-0-nuclearandroidproject-t3247658";
    public static final String linkNAPGitHub = "http://github.com/NuclearAndroidProject1";


    public static final String linkSlimKernel ="https://www.androidfilehost.com/?w=files&flid=32501";
    public static final String linkSlimKernelHtc = "http://www.htcmania.com/showthread.php?t=1055872";
    public static final String linkSlimKernelXDA = "http://forum.xda-developers.com/oneplus-one/development/radioactivekernel-team-nuclear-t3119830";
    public static final String linkAcuicultor ="https://github.com/acuicultor/Radioactive-kernel";

    public static final String linkSlimCAFKernel ="https://www.androidfilehost.com/?w=files&flid=40454";
    public static final String linkSlimCAFKernelXDA = "http://forum.xda-developers.com/oneplus-one/development/radioactive-caf-kerneluber5-2-1cortex-t3213528";
    public static final String linkAcuicultorLollipopCAF ="https://github.com/acuicultor/Radioactive-kernel-caf";


    public static final String linkRebornKernel ="https://www.androidfilehost.com/?w=files&flid=46153";
    public static final String linkRebornKernelXDA = "http://forum.xda-developers.com/oneplus-one/development/radioactivekernel-team-nuclear-t3119830";
    public static final String linkAcuicultorReborn ="http://goo.gl/OOuyfj";

    public static final String linkCafKernel ="https://www.androidfilehost.com/?w=files&flid=43635";
    public static final String linkCafKernelXDA = "http://forum.xda-developers.com/oneplus-one/development/radioactive-kernel-aosp-mm-t3247460";
    public static final String linkAcuicultorCaf ="https://goo.gl/EdhKmT";

    public static final String recoveryOneplusOne = "https://www.androidfilehost.com/?w=files&flid=50417";
    public static final String firmwareOneplusOne = "https://www.androidfilehost.com/?w=files&flid=49097";
    public static final String recoveryzukz1 = "Proximamente";
    public static final String firmwarezukz1 = "Proximamente";
    public static final String appWallpaper = "https://play.google.com/store/apps/details?id=com.delozoya.nuclearwallpaper&hl=es";



}
