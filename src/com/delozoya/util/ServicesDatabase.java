package com.delozoya.util;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by David on 28/02/2016.
 */
public class ServicesDatabase {

    public static boolean repeat;
    public static ArrayList<String> ids = new ArrayList<String>();
    public ServicesDatabase(){}

    public static String ip = "62.43.193.254";

    public static ArrayList<String> getIds() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/NuclearBot", BuildVars.usernamedatabase, BuildVars.passdatabase);
        /*Statement s = conexion.createStatement();
        ResultSet rs = s.executeQuery("select * from id_mensajes.id ");*/

        String SQLEjecutar ="select * from NuclearBot.startBotUser ";
        String resultadoSQL = "";
        ids.clear();
        repeat=false;
        //ejecutamos consulta SQL de selección (devuelve datos)
        Statement st = conexion.createStatement();
        ResultSet rs = st.executeQuery(SQLEjecutar);

        int numColumnas = rs.getMetaData().getColumnCount();

        System.out.println(numColumnas);



        while (rs.next())
        {
            resultadoSQL = resultadoSQL + " ";
            //obtenemos los datos de cada columna
            for (int i = 1; i <= numColumnas; i++)
            {
                if (rs.getObject(i) != null) {

                    if (resultadoSQL != "") {
                        if (i < numColumnas) {
                            resultadoSQL = resultadoSQL +
                                    rs.getObject(i).toString() + ";";
                            ids.add(rs.getObject(i).toString());
                        } else {
                            resultadoSQL = resultadoSQL +
                                    rs.getObject(i).toString();
                            ids.add(rs.getObject(i).toString());
                        }
                    }else {
                        if (i < numColumnas) {
                            resultadoSQL = rs.getObject(i).toString() + ";";
                            ids.add(rs.getObject(i).toString());
                        }else {
                            resultadoSQL = rs.getObject(i).toString();
                            ids.add(rs.getObject(i).toString());
                        }
                    }
                }else{
                    if (resultadoSQL != "") {
                        resultadoSQL = resultadoSQL + "null;";
                        ids.add(rs.getObject(i).toString());
                    }else {
                        resultadoSQL = "null;";
                    }
                }
            }
            resultadoSQL = resultadoSQL + " ";
        }
        System.out.println(resultadoSQL);


        st.close();
        rs.close();
        return ids;
    }

    public static Boolean addID(String id, String id_name) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/NuclearBot", BuildVars.usernamedatabase, BuildVars.passdatabase);

        getIds();
        Statement st = conexion.createStatement();


        for(int j =0;j<ids.size();j++){
            System.out.println(ids.get(j));
            if(ids.get(j).equals(String.valueOf(id))){
                System.out.println("serepite");
                repeat=true;
            }
        }
        if(repeat){
            return true;
        }else {
            st.executeUpdate("INSERT INTO NuclearBot.startBotUser (idUser, nameUser) VALUES ('"+id+"','@"+id_name+"');");
            return false;
        }
    }

    public static void setIdioma(String id, String idioma){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Connection conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/NuclearBot", BuildVars.usernamedatabase, BuildVars.passdatabase);

        Statement st = conexion.createStatement();

        st.executeUpdate("INSERT INTO NuclearBot.idiomaBotNuclear (iduser, idioma) VALUES('"+id+"', '"+idioma+"') ON DUPLICATE KEY UPDATE idioma='"+idioma+"';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getUserLanguage(Integer userId) {
        String languageCode = "en";
        try {
            Connection conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/NuclearBot", BuildVars.usernamedatabase, BuildVars.passdatabase);

            Statement st = conexion.createStatement();


            ResultSet rs = st.executeQuery("SELECT idioma FROM NuclearBot.idiomaBotNuclear WHERE iduser = '"+userId+"' ;");


            if (rs.next()) {
                languageCode = rs.getString("idioma");
                System.out.println(languageCode);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return languageCode;
    }

    public static void setContador(String idContador){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Connection conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/NuclearBot", BuildVars.usernamedatabase, BuildVars.passdatabase);

            Statement st = conexion.createStatement();

            st.executeUpdate("UPDATE NuclearBot.Contador SET valor = valor +1  WHERE idContador='"+idContador+"';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
