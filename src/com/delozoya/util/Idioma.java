package com.delozoya.util;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Idioma {

    public static String getString(String key, String idioma){
        switch (key){
            case "start":
                if(idioma.equals("ES")) return ResourcesStrings.startES;
                if(idioma.equals("EN")) return ResourcesStrings.startEN;

            case "idioma":
                if(idioma.equals("ES")) return ResourcesStrings.idiomaES;
                if(idioma.equals("EN")) return ResourcesStrings.idiomaEN;

            case "OneplusOne":
                if(idioma.equals("ES")) return ResourcesStrings.OneplusOneES;
                if(idioma.equals("EN")) return ResourcesStrings.OneplusOneEN;

            case "versionAndroid":
                if(idioma.equals("ES")) return ResourcesStrings.versionAndroidES;
                if(idioma.equals("EN")) return ResourcesStrings.versionAndroidEN;

            case "necesitas":
                if(idioma.equals("ES")) return ResourcesStrings.necesitasES;
                if(idioma.equals("EN")) return ResourcesStrings.necesitasEN;

            case "Cancel":
                if(idioma.equals("ES")) return ResourcesStrings.cancelES;
                if(idioma.equals("EN")) return ResourcesStrings.cancelEN;

            case "Other":
                if(idioma.equals("ES")) return ResourcesStrings.otherES;
                if(idioma.equals("EN")) return ResourcesStrings.otherEN;

            case "About":
                if(idioma.equals("ES")) return ResourcesStrings.aboutES;
                if(idioma.equals("EN")) return ResourcesStrings.aboutEN;

            case "Back to":
                if(idioma.equals("ES")) return ResourcesStrings.backES;
                if(idioma.equals("EN")) return ResourcesStrings.backEN;

            case "Language":
                if(idioma.equals("ES")) return ResourcesStrings.cambiaridiomaES;
                if(idioma.equals("EN")) return ResourcesStrings.cambiaridiomaEN;
        }

        return "";
    }
}