package com.delozoya.util;

/**
 * @author Ruben Bermudez
 * @version 1.0
 * @brief Command for the bots
 * @date 20 of June of 2015
 */
public class Commands {
    public static final String commandInitChar = "/";
    /// Help command
    public static final String help = commandInitChar + "help";
    /// Upload command
    public static final String uploadCommand = commandInitChar + "upload";
    /// Start command
    public static final String startCommand = commandInitChar + "start";


}
