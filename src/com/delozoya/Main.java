package com.delozoya;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;

public class Main {

    public static void main(String[] args) {

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new BotTeam());
            System.out.println("Bot iniciado con exito");
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
